import './weatherCard.scss';
import Temperature from '../temperature/Temperature';

// todo: move this to new component
const weatherType = (props) => {
  if (props === 'Clear') {
    return (
      <div className="icon sunny">
        <div className="sun">
          <div className="rays"></div>
        </div>
        <span className="sr-only">sun icon</span>
      </div>
    );
  }

  if (
    props === 'Clouds' ||
    props === 'Smoke' ||
    props === 'Haze' ||
    props === 'Dust' ||
    props === 'Fog' ||
    props === 'Sand' ||
    props === 'Ash' ||
    props === 'Squall'
  ) {
    return (
      <div className="icon cloudy">
        <div className="cloud"></div>
        <div className="cloud"></div>
        <span className="sr-only">cloud icon</span>
      </div>
    );
  }

  if (props === 'Rain' || props === 'Drizzle' || props === 'Mist') {
    return (
      <div className="icon rainy">
        <div className="cloud"></div>
        <div className="rain"></div>
        <span className="sr-only">rain icon</span>
      </div>
    );
  }
  if (props === 'Thunderstorm' || props === 'Tornado') {
    return (
      <div className="icon thunder-storm">
        <div className="cloud"></div>
        <div className="lightning">
          <div className="bolt"></div>
          <div className="bolt"></div>
          <span className="sr-only">thunderstorm icon</span>
        </div>
      </div>
    );
  }

  if (props === 'Snow') {
    return (
      <div className="icon thunder-storm">
        <div className="cloud"></div>
        <div className="lightning">
          <div className="bolt"></div>
          <div className="bolt"></div>
          <span className="sr-only">snow icon</span>
        </div>
      </div>
    );
  } else {
    return '';
  }
};
const WeatherCard = (props) => {
  return (
    <>
      {props && props.weather !== undefined && (
        <div className="weather-container">
          <div className="weather-body">
            <div id="city-tag" aria-label={'city name' + props.name}>
              {props.name}
            </div>
            <div id="icon-tag">{weatherType(props.weather[0].main)}</div>
            <div
              id="type-tag"
              aria-label={'weather forcast ' + props.weather[0].main}
            >
              {props.weather[0].main}
            </div>
            <div id="info-tag">
              <ul className="list-grid">
                <li aria-label={'humidity' + props.main.humidity + 'percent'}>
                  <Temperature
                    temp={props.main.humidity}
                    isSmall={true}
                    title={'Humidity:'}
                    isPercent={true}
                  />
                </li>
                <li
                  aria-label={
                    'max temperature' +
                    props.main.temp_max.toFixed(0) +
                    'degrees'
                  }
                >
                  <Temperature
                    temp={props.main.temp_max}
                    isSmall={true}
                    title={'Max:'}
                  />
                </li>
                <li
                  aria-label={
                    'min temperature' +
                    props.main.temp_min.toFixed(0) +
                    'degrees'
                  }
                >
                  <Temperature
                    temp={props.main.temp_min}
                    isSmall={true}
                    title={'Min:'}
                  />
                </li>
              </ul>
            </div>

            <div
              id="degree-tag"
              aria-label={
                'current temperature' + props.main.temp.toFixed(0) + 'degrees'
              }
            >
              <Temperature temp={props.main.temp} />
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default WeatherCard;
