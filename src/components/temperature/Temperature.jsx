// import React from 'react';
import './temperature.scss';

const Temperature = ({ temp, isSmall, title, isPercent }) => {
  return (
    <>
      <div className="temp-grid">
        {title ? <span className="title">{title}</span> : <span></span>}
        <div className="temp-number">{temp.toFixed(0)}</div>
        {isPercent ? (
          <div className="percent-icon">%</div>
        ) : (
          <div className={`circle ${isSmall ? 'small' : ''}`}></div>
        )}
      </div>
    </>
  );
};

export default Temperature;
