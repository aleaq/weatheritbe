import './App.scss';
import { useEffect, useState, useRef } from 'react';
import axios from 'axios';

// components
import WeatherCard from './components/card/WeatherCard';

function App() {
  const [location, setLocation] = useState('Washington DC');
  const [weather, setWeather] = useState([]);
  const [inputError, setError] = useState(false);
  const [helperText, setHelperText] = useState(false);
  const apiKey = process.env.REACT_APP_API_KEY;
  const apiUrl = 'https://api.openweathermap.org/data/2.5/weather?q=';
  const apiSuffex = `&units=imperial&appid=${apiKey}`;

  // custom hook for previous value
  const usePrevious = (value) => {
    const reference = useRef();
    useEffect(() => {
      reference.current = value;
    });
    return reference.current;
  };

  const prevLocation = usePrevious(location);

  /*** in case the api runs out. I still want to show data ***/
  const demoData = {
    name: 'Washington D.C.',
    main: {
      temp: 85.8,
      temp_max: 90.9,
      temp_min: 67.9,
      humidity: 50,
    },
    weather: [{ main: 'Clear' }],
  };

  const getWeather = async () => {
    if (prevLocation === location) {
      setHelperText(true);
    } else {
      setHelperText(false);
      const url = apiUrl + location + apiSuffex;
      axios
        .get(url)
        .then((res) => {
          if (res.status === 200) {
            const allData = res.data;
            setWeather(allData);
            setError('');
          }
        })
        .catch((err) => {
          if (err.response.status === 404) {
            const message = 'Oops! We need a valid city name';
            setError(message);
          }
          if (err.response.status === 429) {
            const message = 'Oops! Too many API calls, here is some demo data';
            setError(message);
            setWeather(demoData);
          }
          if (err.response.status === 400) {
            const message = 'Enter city name: example - "Sydney"';
            setError(message);
            setWeather(demoData);
          }
          console.log('api error', err);
        });
    }
  };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => getWeather(), []);

  const setCity = (e) => {
    const target = e.target.value;
    setLocation(target);
  };

  const onEnter = (e) => {
    if (e.key === 'Enter') {
      getWeather();
    }
  };
  return (
    <div className="App">
      <main>
        <section>
          <div className="location-input">
            {inputError && <div className="error">{inputError}</div>}
            <input
              autoComplete="off"
              type="text"
              placeholder="Enter city name"
              onChange={setCity}
              value={location}
              onKeyDown={onEnter}
              aria-label="enter city name"
              tabIndex="0"
            />
            <button id="weather-button" type="button" onClick={getWeather}>
              Check Weather
            </button>
          </div>
          <p
            style={{ visibility: helperText ? 'visible' : 'hidden' }}
            className="helper-text"
          >
            update the city name
          </p>
          {weather && (
            <div className="weather-card-container">
              <WeatherCard {...weather} />
            </div>
          )}
        </section>
      </main>
    </div>
  );
}

export default App;
